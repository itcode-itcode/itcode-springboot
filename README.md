#### 介绍

- 程序源代码，一名程序员，主要是java/小程序开发
- 发布平台：公众号/视频号，同名为“程序源代码”

#### 联系方式
- 微信：itcoder
![输入图片说明](qrcode_for_gh_dd93f80f571a_258.jpg)

#### 框架介绍
SpringBoot敏捷开发框架2.0版

#### 软件架构
1. 控制层 Spring Framework4.2
2. 安全框架：Apache Shiro1.3
3. 视图框架：SpringMVC4.2
4. 持久层框架：Mybaits3.3
5. 数据库连接池：Druid1.0
6. 页面交互：vue2.0
7. 前后端分离：Swagger2
8. 定时任务：Quartz2.3
9. 代码生成器：Veloctiy1.7

#### 安装教程
1. 本地准备开发工具IDE，建议使用eclipse 或者springSTS
2. 安装JDK1.8，并配置环境变量
3. 安装maven，并配置本地仓库
4. 安装mysql5.X并配置

#### 使用说明
1. 使用IDE，导入maven工程或者GIT仓库源码
2. 使用数据库工具创建数据库，名称：shiro_boot,字符utf-8 .导入db.sql文件
3. IDE更新资源文件和配置
4. 运行入口java文件
4. 访问http://localhost  用户名 admin  密码123456

#### 运行图
![输入图片说明](https://images.gitee.com/uploads/images/2019/0521/190648_65c209da_374985.png "微信截图_20190521182526.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0521/190638_c6ee3014_374985.png "微信截图_20190521182558.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0521/190657_8e3cc2e8_374985.png "微信截图_20190521183033.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0521/190706_40e71ba4_374985.png "微信截图_20190521183045.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0521/190713_7d5f1a65_374985.png "微信截图_20190521183056.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0521/190720_03a7d91e_374985.png "微信截图_20190521183108.png")