package com.cf.dao;

import com.cf.entity.UserEntity;

/**
 * 功能描述：用户DAO
 * @author 公众号：程序源代码
 * @date 2019年5月21日
 */
public interface UserDao extends BaseDao<UserEntity> {

    UserEntity queryByMobile(String mobile);
}
