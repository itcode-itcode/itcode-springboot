package com.cf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cf.entity.SysUserEntity;
import com.cf.utils.ShiroUtils;

/**
 * 功能描述：Controller公共组件
 * @author 公众号：程序源代码
 * @date 2019年5月21日
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected SysUserEntity getUser() {
		return ShiroUtils.getUserEntity();
	}

	protected Long getUserId() {
		return getUser().getUserId();
	}
}
