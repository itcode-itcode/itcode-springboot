package com.cf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能描述：主程序入口
 * 
 * @author 公众号：程序源代码
 * @date 2019年5月21日
 */
@SpringBootApplication
public class CfApplication {

	public static void main(String[] args) {
		//主程序入口
		SpringApplication.run(CfApplication.class, args);
	}
}
