package com.cf.service;

import java.util.List;

/**
 * 功能描述： 角色与菜单对应关系
 * @author 公众号：程序源代码
 * @date 2019年5月21日
 */
public interface SysRoleMenuService {
	
	void saveOrUpdate(Long roleId, List<Long> menuIdList);
	
	/**
	 * 根据角色ID，获取菜单ID列表
	 */
	List<Long> queryMenuIdList(Long roleId);
	
}
