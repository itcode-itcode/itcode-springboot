package com.cf.service;

import java.util.List;
import java.util.Map;

import com.cf.entity.ScheduleJobLogEntity;

/**
 * 定时任务日志
 * 
 */
/**
 * 功能描述：
 * @author 公众号：程序源代码
 * @date 2019年5月21日
 */
public interface ScheduleJobLogService {

	/**
	 * 根据ID，查询定时任务日志
	 */
	ScheduleJobLogEntity queryObject(Long jobId);
	
	/**
	 * 查询定时任务日志列表
	 */
	List<ScheduleJobLogEntity> queryList(Map<String, Object> map);
	
	/**
	 * 查询总数
	 */
	int queryTotal(Map<String, Object> map);
	
	/**
	 * 保存定时任务日志
	 */
	void save(ScheduleJobLogEntity log);
	
}
