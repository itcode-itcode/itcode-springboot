package com.cf.service;

import java.util.List;
/**
 * 功能描述：用户与角色对应关系
 * 
 * @author 公众号：程序源代码
 * @date 2019年5月21日
 */
public interface SysUserRoleService {
	
	void saveOrUpdate(Long userId, List<Long> roleIdList);
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryRoleIdList(Long userId);
	
	void delete(Long userId);
}
