package com.cf.service;

import java.util.List;
import java.util.Map;

import com.cf.entity.SysRoleEntity;

/**
 * 功能描述：角色
 * @author 公众号：程序源代码
 * @date 2019年5月21日
 */
public interface SysRoleService {
	
	SysRoleEntity queryObject(Long roleId);
	
	List<SysRoleEntity> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(SysRoleEntity role);
	
	void update(SysRoleEntity role);
	
	void deleteBatch(Long[] roleIds);
	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(Long createUserId);
}
