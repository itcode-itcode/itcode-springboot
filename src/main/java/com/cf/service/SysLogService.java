package com.cf.service;

import java.util.List;
import java.util.Map;

import com.cf.entity.SysLogEntity;

/**
 * 功能描述：系统日志
 * @author 公众号：程序源代码
 * @date 2019年5月21日
 */
public interface SysLogService {
	
	SysLogEntity queryObject(Long id);
	
	List<SysLogEntity> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(SysLogEntity sysLog);
	
	void update(SysLogEntity sysLog);
	
	void delete(Long id);
	
	void deleteBatch(Long[] ids);
}
