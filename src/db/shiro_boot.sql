/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50643
Source Host           : localhost:3306
Source Database       : shiro_boot

Target Server Type    : MYSQL
Target Server Version : 50643
File Encoding         : 65001

Date: 2020-03-12 11:11:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `equipment`
-- ----------------------------
DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment` (
  `name` varchar(100) DEFAULT NULL COMMENT '设备名称',
  `brand` varchar(100) DEFAULT NULL COMMENT '品牌',
  `Model` varchar(100) DEFAULT NULL COMMENT '型号',
  `softwareversion` varchar(100) DEFAULT NULL COMMENT '软件版本',
  `address` varchar(500) DEFAULT NULL COMMENT ' 位置',
  `year` int(3) DEFAULT NULL COMMENT '年限',
  `status` varchar(100) DEFAULT NULL COMMENT ' 维保状态',
  `sid` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '序号',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='设备信息管理';

-- ----------------------------
-- Records of equipment
-- ----------------------------
INSERT INTO `equipment` VALUES ('联想服务器D100', '联想', 'D100', '1', '中心机房2-1-1-100', '10', '正常', '5');

-- ----------------------------
-- Table structure for `equipmenttype`
-- ----------------------------
DROP TABLE IF EXISTS `equipmenttype`;
CREATE TABLE `equipmenttype` (
  `tid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(100) DEFAULT NULL COMMENT '类型名称',
  `bz` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='设备类型';

-- ----------------------------
-- Records of equipmenttype
-- ----------------------------
INSERT INTO `equipmenttype` VALUES ('1', '路由器', null);

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('CfScheduler', 'TASK_1', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('CfScheduler', 'TASK_2', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('CfScheduler', 'TASK_1', 'DEFAULT', null, 'com.cf.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597400A87B226265616E4E616D65223A22746573745461736B222C2263726561746554696D65223A313439363333303230363030302C2263726F6E45787072657373696F6E223A223020302F3330202A202A202A203F222C226A6F624964223A312C226D6574686F644E616D65223A2274657374222C22706172616D73223A2270617261222C2272656D61726B223A22E69C89E58F82E695B0E6B58BE8AF95222C22737461747573223A307D7800);
INSERT INTO `qrtz_job_details` VALUES ('CfScheduler', 'TASK_2', 'DEFAULT', null, 'com.cf.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597400997B226265616E4E616D65223A22746573745461736B222C2263726561746554696D65223A313439363437323935363030302C2263726F6E45787072657373696F6E223A223020302F3330202A202A202A203F222C226A6F624964223A322C226D6574686F644E616D65223A227465737432222C2272656D61726B223A22E697A0E58F82E695B0E6B58BE8AF95222C22737461747573223A317D7800);

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('CfScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('CfScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('CfScheduler', 'quanhua-PC1558429297299', '1558434823274', '15000');

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('CfScheduler', 'TASK_1', 'DEFAULT', 'TASK_1', 'DEFAULT', null, '1521441000000', '-1', '5', 'WAITING', 'CRON', '1521439962000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597400A87B226265616E4E616D65223A22746573745461736B222C2263726561746554696D65223A313439363333303230363030302C2263726F6E45787072657373696F6E223A223020302F3330202A202A202A203F222C226A6F624964223A312C226D6574686F644E616D65223A2274657374222C22706172616D73223A2270617261222C2272656D61726B223A22E69C89E58F82E695B0E6B58BE8AF95222C22737461747573223A307D7800);
INSERT INTO `qrtz_triggers` VALUES ('CfScheduler', 'TASK_2', 'DEFAULT', 'TASK_2', 'DEFAULT', null, '1521441000000', '-1', '5', 'PAUSED', 'CRON', '1521439962000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597400997B226265616E4E616D65223A22746573745461736B222C2263726561746554696D65223A313439363437323935363030302C2263726F6E45787072657373696F6E223A223020302F3330202A202A202A203F222C226A6F624964223A322C226D6574686F644E616D65223A227465737432222C2272656D61726B223A22E697A0E58F82E695B0E6B58BE8AF95222C22737461747573223A317D7800);

-- ----------------------------
-- Table structure for `schedule_job`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('1', 'testTask', 'test', 'para', '0 0/30 * * * ?', '0', '有参数测试', '2017-06-01 23:16:46');
INSERT INTO `schedule_job` VALUES ('2', 'testTask', 'test2', null, '0 0/30 * * * ?', '1', '无参数测试', '2017-06-03 14:55:56');

-- ----------------------------
-- Table structure for `schedule_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES ('1', '1', 'testTask', 'test', 'para', '0', null, '1031', '2018-03-19 14:30:01');
INSERT INTO `schedule_job_log` VALUES ('2', '1', 'testTask', 'test', 'para', '0', null, '1056', '2018-03-19 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('3', '1', 'testTask', 'test', 'para', '0', null, '1006', '2018-03-19 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('4', '1', 'testTask', 'test', 'para', '0', null, '1058', '2018-03-19 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('5', '1', 'testTask', 'test', 'para', '0', null, '1004', '2018-03-19 16:30:01');
INSERT INTO `schedule_job_log` VALUES ('6', '1', 'testTask', 'test', 'para', '0', null, '1004', '2018-03-19 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('7', '1', 'testTask', 'test', 'para', '0', null, '1033', '2018-03-26 10:30:01');
INSERT INTO `schedule_job_log` VALUES ('8', '1', 'testTask', 'test', 'para', '0', null, '1006', '2018-03-26 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('9', '1', 'testTask', 'test', 'para', '0', null, '1006', '2018-03-26 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('10', '1', 'testTask', 'test', 'para', '0', null, '1033', '2018-03-26 13:30:07');
INSERT INTO `schedule_job_log` VALUES ('11', '1', 'testTask', 'test', 'para', '0', null, '1025', '2018-03-26 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('12', '1', 'testTask', 'test', 'para', '0', null, '1030', '2018-03-26 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('13', '1', 'testTask', 'test', 'para', '0', null, '1694', '2018-03-26 15:30:02');
INSERT INTO `schedule_job_log` VALUES ('14', '1', 'testTask', 'test', 'para', '0', null, '1210', '2018-03-27 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('15', '1', 'testTask', 'test', 'para', '0', null, '1034', '2018-03-27 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('16', '1', 'testTask', 'test', 'para', '0', null, '1024', '2018-03-27 16:00:01');
INSERT INTO `schedule_job_log` VALUES ('17', '1', 'testTask', 'test', 'para', '0', null, '1029', '2018-03-27 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('18', '1', 'testTask', 'test', 'para', '0', null, '1004', '2018-03-27 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('19', '1', 'testTask', 'test', 'para', '0', null, '1004', '2018-03-27 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('20', '1', 'testTask', 'test', 'para', '0', null, '1085', '2019-05-18 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('21', '1', 'testTask', 'test', 'para', '0', null, '1023', '2019-05-18 21:00:01');
INSERT INTO `schedule_job_log` VALUES ('22', '1', 'testTask', 'test', 'para', '0', null, '1060', '2019-05-18 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('23', '1', 'testTask', 'test', 'para', '0', null, '1036', '2019-05-21 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('24', '1', 'testTask', 'test', 'para', '0', null, '1042', '2019-05-21 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('25', '1', 'testTask', 'test', 'para', '0', null, '1016', '2019-05-21 18:00:00');
INSERT INTO `schedule_job_log` VALUES ('26', '1', 'testTask', 'test', 'para', '0', null, '1008', '2019-05-21 18:30:01');

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) DEFAULT NULL COMMENT 'key',
  `value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', '0', '云存储配置信息');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1', 'admin', '保存定时任务', 'com.cf.controller.ScheduleJobController.save()', '{}', '0:0:0:0:0:0:0:1', '2018-03-19 14:34:29');
INSERT INTO `sys_log` VALUES ('2', 'admin', '保存角色', 'com.cf.controller.SysRoleController.save()', '{\"menuIdList\":[1,2,15,16,17,18],\"remark\":\"测试\",\"roleName\":\"二级管理员\"}', '0:0:0:0:0:0:0:1', '2018-03-19 14:36:35');
INSERT INTO `sys_log` VALUES ('3', 'admin', '保存用户', 'com.cf.controller.SysUserController.save()', '{\"email\":\"1\",\"mobile\":\"11\",\"roleIdList\":[1],\"status\":1,\"username\":\"yanchun\"}', '0:0:0:0:0:0:0:1', '2018-03-19 14:36:56');
INSERT INTO `sys_log` VALUES ('4', 'admin', '保存用户', 'com.cf.controller.SysUserController.save()', '{\"email\":\"123@163.com\",\"mobile\":\"11\",\"roleIdList\":[1],\"status\":1,\"username\":\"yanchun\"}', '0:0:0:0:0:0:0:1', '2018-03-19 14:37:10');
INSERT INTO `sys_log` VALUES ('5', 'admin', '保存菜单', 'com.cf.controller.SysMenuController.save()', '{\"name\":\"测试\",\"orderNum\":0,\"parentId\":0,\"parentName\":\"一级菜单\",\"type\":1}', '0:0:0:0:0:0:0:1', '2018-03-19 14:38:34');
INSERT INTO `sys_log` VALUES ('6', 'admin', '保存菜单', 'com.cf.controller.SysMenuController.save()', '{\"name\":\"测试\",\"orderNum\":0,\"parentId\":0,\"parentName\":\"一级菜单\",\"type\":1,\"url\":\"1\"}', '0:0:0:0:0:0:0:1', '2018-03-19 14:38:40');
INSERT INTO `sys_log` VALUES ('7', 'admin', '删除菜单', 'com.cf.controller.SysMenuController.delete()', '[28]', '0:0:0:0:0:0:0:1', '2018-03-19 14:50:01');
INSERT INTO `sys_log` VALUES ('8', 'admin', '删除菜单', 'com.cf.controller.SysMenuController.delete()', '[30]', '0:0:0:0:0:0:0:1', '2018-03-19 14:50:54');
INSERT INTO `sys_log` VALUES ('9', 'admin', '删除菜单', 'com.cf.controller.SysMenuController.delete()', '[30]', '0:0:0:0:0:0:0:1', '2018-03-19 14:51:38');
INSERT INTO `sys_log` VALUES ('10', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-user\",\"menuId\":2,\"name\":\"学生信息管理\",\"orderNum\":1,\"parentId\":1,\"parentName\":\"系统管理\",\"type\":1,\"url\":\"sys/user.html\"}', '0:0:0:0:0:0:0:1', '2018-03-19 14:55:58');
INSERT INTO `sys_log` VALUES ('11', 'yanchun', '保存用户', 'com.cf.controller.SysUserController.save()', '{\"email\":\"zhangsan@163.com\",\"mobile\":\"123445555\",\"roleIdList\":[],\"status\":1,\"username\":\"zhangsan\"}', '0:0:0:0:0:0:0:1', '2018-03-19 14:56:48');
INSERT INTO `sys_log` VALUES ('12', 'yanchun', '保存用户', 'com.cf.controller.SysUserController.save()', '{\"email\":\"1@123\",\"mobile\":\"1\",\"roleIdList\":[],\"status\":1,\"username\":\"zhangsan111\"}', '0:0:0:0:0:0:0:1', '2018-03-19 14:57:06');
INSERT INTO `sys_log` VALUES ('13', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-user\",\"menuId\":2,\"name\":\"用户管理\",\"orderNum\":1,\"parentId\":1,\"parentName\":\"系统管理\",\"type\":1,\"url\":\"sys/user.html\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:26:43');
INSERT INTO `sys_log` VALUES ('14', 'admin', '删除用户', 'com.cf.controller.SysUserController.delete()', '[2,3,4]', '0:0:0:0:0:0:0:1', '2018-03-26 10:27:41');
INSERT INTO `sys_log` VALUES ('15', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-bug\",\"menuId\":5,\"name\":\"性能监控\",\"orderNum\":4,\"parentId\":1,\"parentName\":\"系统管理\",\"type\":1,\"url\":\"druid/sql.html\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:28:04');
INSERT INTO `sys_log` VALUES ('16', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-tasks\",\"menuId\":6,\"name\":\"任务管理\",\"orderNum\":5,\"parentId\":1,\"parentName\":\"系统管理\",\"type\":1,\"url\":\"sys/schedule.html\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:28:13');
INSERT INTO `sys_log` VALUES ('17', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-file-text-o\",\"menuId\":29,\"name\":\"日志管理\",\"orderNum\":7,\"parentId\":1,\"parentName\":\"系统管理\",\"perms\":\"sys:log:list\",\"type\":1,\"url\":\"sys/log.html\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:28:25');
INSERT INTO `sys_log` VALUES ('18', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-rocket\",\"menuId\":28,\"name\":\"代码管理\",\"orderNum\":8,\"parentId\":1,\"parentName\":\"系统管理\",\"perms\":\"sys:generator:list,sys:generator:code\",\"type\":1,\"url\":\"sys/generator.html\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:28:39');
INSERT INTO `sys_log` VALUES ('19', 'admin', '删除菜单', 'com.cf.controller.SysMenuController.delete()', '[29]', '0:0:0:0:0:0:0:1', '2018-03-26 10:29:08');
INSERT INTO `sys_log` VALUES ('20', 'admin', '删除菜单', 'com.cf.controller.SysMenuController.delete()', '[30]', '0:0:0:0:0:0:0:1', '2018-03-26 10:31:50');
INSERT INTO `sys_log` VALUES ('21', 'admin', '修改用户', 'com.cf.controller.SysUserController.update()', '{\"createTime\":1496302400000,\"email\":\"admin@test.com\",\"mobile\":\"13666666666\",\"roleIdList\":[],\"status\":1,\"userId\":1,\"username\":\"admin\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:32:52');
INSERT INTO `sys_log` VALUES ('22', 'admin', '修改用户', 'com.cf.controller.SysUserController.update()', '{\"createTime\":1496302400000,\"email\":\"admin@qq.com\",\"mobile\":\"13666666666\",\"roleIdList\":[],\"status\":1,\"userId\":1,\"username\":\"admin\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:33:07');
INSERT INTO `sys_log` VALUES ('23', 'admin', '修改角色', 'com.cf.controller.SysRoleController.update()', '{\"createTime\":1521441395000,\"createUserId\":1,\"menuIdList\":[],\"remark\":\"普通用户组\",\"roleId\":1,\"roleName\":\"普通用户\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:38:17');
INSERT INTO `sys_log` VALUES ('24', 'admin', '保存角色', 'com.cf.controller.SysRoleController.save()', '{\"menuIdList\":[1,2,15,16,17,18,3,19,20,21,22,4,23,24,25,26],\"remark\":\"管理员\",\"roleName\":\"管理员\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:38:42');
INSERT INTO `sys_log` VALUES ('25', 'admin', '保存角色', 'com.cf.controller.SysRoleController.save()', '{\"menuIdList\":[],\"remark\":\"测试用户\",\"roleName\":\"测试用户\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:39:03');
INSERT INTO `sys_log` VALUES ('26', 'admin', '保存用户', 'com.cf.controller.SysUserController.save()', '{\"email\":\"123@qq.com\",\"mobile\":\"18999999999\",\"roleIdList\":[1],\"status\":1,\"username\":\"test\"}', '0:0:0:0:0:0:0:1', '2018-03-26 10:40:00');
INSERT INTO `sys_log` VALUES ('27', 'admin', '保存菜单', 'com.cf.controller.SysMenuController.save()', '{\"icon\":\"fa fa-cog\",\"name\":\"设备管理\",\"orderNum\":0,\"parentId\":0,\"parentName\":\"一级菜单\",\"type\":0}', '0:0:0:0:0:0:0:1', '2018-03-26 11:30:27');
INSERT INTO `sys_log` VALUES ('28', 'admin', '修改角色', 'com.cf.controller.SysRoleController.update()', '{\"createTime\":1521441395000,\"createUserId\":1,\"menuIdList\":[31],\"remark\":\"普通用户组\",\"roleId\":1,\"roleName\":\"普通用户\"}', '0:0:0:0:0:0:0:1', '2018-03-26 13:44:14');
INSERT INTO `sys_log` VALUES ('29', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-file-code-o\",\"menuId\":32,\"name\":\"设备信息管理\",\"orderNum\":6,\"parentId\":31,\"parentName\":\"设备管理\",\"type\":1,\"url\":\"sys/equipment.html\"}', '0:0:0:0:0:0:0:1', '2018-03-26 13:45:12');
INSERT INTO `sys_log` VALUES ('30', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-th-list\",\"menuId\":32,\"name\":\"设备信息管理\",\"orderNum\":6,\"parentId\":31,\"parentName\":\"设备管理\",\"perms\":\"\",\"type\":1,\"url\":\"sys/equipment.html\"}', '0:0:0:0:0:0:0:1', '2018-03-27 16:07:56');
INSERT INTO `sys_log` VALUES ('31', 'admin', '修改角色', 'com.cf.controller.SysRoleController.update()', '{\"createTime\":1521441395000,\"createUserId\":1,\"menuIdList\":[31,32,33,34,35,36],\"remark\":\"普通用户组\",\"roleId\":1,\"roleName\":\"普通用户\"}', '0:0:0:0:0:0:0:1', '2018-03-27 16:08:26');
INSERT INTO `sys_log` VALUES ('32', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-file-code-o\",\"menuId\":37,\"name\":\"设备类型\",\"orderNum\":6,\"parentId\":31,\"parentName\":\"设备管理\",\"type\":1,\"url\":\"sys/equipmenttype.html\"}', '0:0:0:0:0:0:0:1', '2018-03-27 16:17:55');
INSERT INTO `sys_log` VALUES ('33', 'admin', '修改菜单', 'com.cf.controller.SysMenuController.update()', '{\"icon\":\"fa fa-file-code-o\",\"menuId\":37,\"name\":\"设备类型\",\"orderNum\":1,\"parentId\":31,\"parentName\":\"设备管理\",\"type\":1,\"url\":\"sys/equipmenttype.html\"}', '0:0:0:0:0:0:0:1', '2018-03-27 16:18:34');
INSERT INTO `sys_log` VALUES ('34', 'admin', '删除菜单', 'com.cf.controller.SysMenuController.delete()', '[41,40,39,38,36,35,34]', '0:0:0:0:0:0:0:1', '2019-05-21 16:11:29');
INSERT INTO `sys_log` VALUES ('35', 'admin', '删除菜单', 'com.cf.controller.SysMenuController.delete()', '[33]', '0:0:0:0:0:0:0:1', '2019-05-21 16:11:38');
INSERT INTO `sys_log` VALUES ('36', 'admin', '删除菜单', 'com.cf.controller.SysMenuController.delete()', '[31,37,32]', '0:0:0:0:0:0:0:1', '2019-05-21 16:12:58');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', 'fa fa-cog', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '用户管理', 'sys/user.html', null, '1', 'fa fa-user', '1');
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', 'sys/role.html', null, '1', 'fa fa-user-secret', '2');
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', 'sys/menu.html', null, '1', 'fa fa-th-list', '3');
INSERT INTO `sys_menu` VALUES ('15', '2', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '2', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '2', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '2', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '3', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '3', '新增', null, 'sys:role:save,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '3', '修改', null, 'sys:role:update,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '3', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '4', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '4', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '4', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '4', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('28', '1', '代码管理', 'sys/generator.html', 'sys:generator:list,sys:generator:code', '1', 'fa fa-rocket', '8');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '普通用户', '普通用户组', '1', '2018-03-19 14:36:35');
INSERT INTO `sys_role` VALUES ('2', '管理员', '管理员', '1', '2018-03-26 10:38:42');
INSERT INTO `sys_role` VALUES ('3', '测试用户', '测试用户', '1', '2018-03-26 10:39:03');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('7', '2', '1');
INSERT INTO `sys_role_menu` VALUES ('8', '2', '2');
INSERT INTO `sys_role_menu` VALUES ('9', '2', '15');
INSERT INTO `sys_role_menu` VALUES ('10', '2', '16');
INSERT INTO `sys_role_menu` VALUES ('11', '2', '17');
INSERT INTO `sys_role_menu` VALUES ('12', '2', '18');
INSERT INTO `sys_role_menu` VALUES ('13', '2', '3');
INSERT INTO `sys_role_menu` VALUES ('14', '2', '19');
INSERT INTO `sys_role_menu` VALUES ('15', '2', '20');
INSERT INTO `sys_role_menu` VALUES ('16', '2', '21');
INSERT INTO `sys_role_menu` VALUES ('17', '2', '22');
INSERT INTO `sys_role_menu` VALUES ('18', '2', '4');
INSERT INTO `sys_role_menu` VALUES ('19', '2', '23');
INSERT INTO `sys_role_menu` VALUES ('20', '2', '24');
INSERT INTO `sys_role_menu` VALUES ('21', '2', '25');
INSERT INTO `sys_role_menu` VALUES ('22', '2', '26');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '100@qq.com', '13666666666', '1', null, '2017-06-01 15:33:20');
INSERT INTO `sys_user` VALUES ('5', 'test', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', '123@qq.com', '18999999999', '1', '1', '2018-03-26 10:40:00');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('2', '5', '1');

-- ----------------------------
-- Table structure for `tb_token`
-- ----------------------------
DROP TABLE IF EXISTS `tb_token`;
CREATE TABLE `tb_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户Token';

-- ----------------------------
-- Records of tb_token
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'test', '13888888888', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');
